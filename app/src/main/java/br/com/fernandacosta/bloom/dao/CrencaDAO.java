package br.com.fernandacosta.bloom.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.fernandacosta.bloom.model.Crenca;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class CrencaDAO {

    private Realm realm = Realm.getDefaultInstance();


    public RealmList<String> getModelList(ArrayList<String> resposta) {
        RealmList<String> teste = new RealmList<>();
        for (String respostaCrencaCentral : resposta) {
            teste.add(respostaCrencaCentral);
        }
        return teste;
    }

    public RealmList<Boolean> getModelListBoolean(ArrayList<String> resposta) {
        RealmList<Boolean> teste = new RealmList<>();
        for (String respostaCrencaCentral : resposta) {
            teste.add(Boolean.getBoolean(respostaCrencaCentral));
        }
        return teste;
    }

    public void adicionar(Crenca crenca){
        realm.beginTransaction();
        realm.copyToRealm(crenca);
        realm.commitTransaction();
    }

    public int getUlitmoId(){
        Number ultimoId = realm.where(Crenca.class).max("id");
        if (ultimoId != null)
            return ultimoId.intValue() + 1;
        else
            return 0;
    }

    public List<Crenca> getTodasCrencas(){
        RealmResults<Crenca> todasCrencas = realm.where(Crenca.class).findAll();
        return realm.copyFromRealm(todasCrencas);
    }

    public Crenca getCrenca(int id){
        return realm.where(Crenca.class).equalTo("id", id).findFirst();
    }


}
