package br.com.fernandacosta.bloom.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import br.com.fernandacosta.bloom.R;
import br.com.fernandacosta.bloom.dao.CrencaDAO;
import br.com.fernandacosta.bloom.model.Crenca;
import br.com.fernandacosta.bloom.ui.recyclerview.OnItemClickListener;
import br.com.fernandacosta.bloom.ui.recyclerview.adapter.DetalhesCrencaAdapter;

import static br.com.fernandacosta.bloom.ui.constant.CrencasConstantes.APPBAR_TITULO_CRENCAS_INTERMEDIARIAS;
import static br.com.fernandacosta.bloom.ui.constant.CrencasConstantes.CRENCA_ID;
import static br.com.fernandacosta.bloom.ui.constant.CrencasConstantes.NUMERO_PERGUNTA;
import static br.com.fernandacosta.bloom.ui.constant.CrencasConstantes.VALOR_INVALIDO;

public class DetalhesCrencaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_crenca);

        Crenca crenca = getCrenca();

        setTitle(APPBAR_TITULO_CRENCAS_INTERMEDIARIAS);

        DetalhesCrencaAdapter adapter = configuraAdapter(crenca);
        configuraRecyclerView(adapter);
    }

    private void configuraRecyclerView(DetalhesCrencaAdapter adapter) {
        RecyclerView respostasRecyclerview = findViewById(R.id.detalhes_crenca_recyclerview);
        respostasRecyclerview.setAdapter(adapter);
    }

    @NonNull
    private DetalhesCrencaAdapter configuraAdapter(Crenca crenca) {
        DetalhesCrencaAdapter adapter = new DetalhesCrencaAdapter(getApplicationContext(), crenca.getRespostasCrencasIntermediarias());
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int posicao) {
                vaiParaAlterarCrencaIntermediaria(posicao);
            }
        });
        return adapter;
    }

    private Crenca getCrenca() {
        Intent intent = getIntent();
        int crenca_id = intent.getIntExtra(CRENCA_ID, VALOR_INVALIDO);

        return new CrencaDAO().getCrenca(crenca_id);
    }

    private void vaiParaAlterarCrencaIntermediaria(int posicao) {
        Intent intent = new Intent(this, CrencasIntermediariasActivity.class);
        intent.putExtra(NUMERO_PERGUNTA, posicao);
        startActivity(intent);
    }
}
