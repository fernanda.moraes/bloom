package br.com.fernandacosta.bloom.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Crenca extends RealmObject implements Serializable {

    @PrimaryKey
    private int id;
    private Date diaQueFoiRealizado;
    private RealmList<Boolean> respostasCrencasCentrais;
    private RealmList<String> respostasCrencasIntermediarias;

    public Crenca() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDiaQueFoiRealizado() {
        return diaQueFoiRealizado;
    }

    public void setDiaQueFoiRealizado(Date diaQueFoiRealizado) {
        this.diaQueFoiRealizado = diaQueFoiRealizado;
    }

    public List<Boolean> getRespostasCrencasCentrais() {
        return respostasCrencasCentrais;
    }

    public void setRespostasCrencasCentrais(RealmList<Boolean> respostasCrencasCentrais) {
        this.respostasCrencasCentrais = respostasCrencasCentrais;
    }

    public List<String> getRespostasCrencasIntermediarias() {
        return respostasCrencasIntermediarias;
    }

    public void setRespostasCrencasIntermediarias(RealmList<String> respostasCrencasIntermediarias) {
        this.respostasCrencasIntermediarias = respostasCrencasIntermediarias;
    }
}
