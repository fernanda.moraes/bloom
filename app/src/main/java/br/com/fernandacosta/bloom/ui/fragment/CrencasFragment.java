package br.com.fernandacosta.bloom.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.fernandacosta.bloom.R;
import br.com.fernandacosta.bloom.dao.CrencaDAO;
import br.com.fernandacosta.bloom.model.Crenca;
import br.com.fernandacosta.bloom.ui.activity.CrencasCentraisActivity;
import br.com.fernandacosta.bloom.ui.activity.DetalhesCrencaActivity;
import br.com.fernandacosta.bloom.ui.recyclerview.OnItemClickListener;
import br.com.fernandacosta.bloom.ui.recyclerview.adapter.CrencasAdapter;

import static br.com.fernandacosta.bloom.ui.constant.CrencasConstantes.APPBAR_TITULO;
import static br.com.fernandacosta.bloom.ui.constant.CrencasConstantes.CRENCA_ID;
import static br.com.fernandacosta.bloom.ui.constant.CrencasConstantes.POSICAO;


public class CrencasFragment extends Fragment {

    private List<Crenca> todasCrencas;
    private CrencasAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_crencas, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle(APPBAR_TITULO);

        todasCrencas = new CrencaDAO().getTodasCrencas();

        configuraCrencasAdicionarFab(view);
        configuraAdapter();
        configuraRecyclerview(view);
    }

    private void configuraRecyclerview(@NonNull View view) {
        RecyclerView crencasRecyclerView = view.findViewById(R.id.crencas_recyclerView);
        crencasRecyclerView.setAdapter(adapter);
    }

    private void configuraAdapter() {
        adapter = new CrencasAdapter(getContext(), todasCrencas);
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int posicao) {
                Crenca crenca = todasCrencas.get(posicao);
                vaiParaDetalhesCrenca(crenca, posicao);
            }
        });
    }

    private void configuraCrencasAdicionarFab(@NonNull View view) {
        FloatingActionButton adicionarFab = view.findViewById(R.id.crencas_adicionar_fab);
        adicionarFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CrencasCentraisActivity.class);
                startActivity(intent);
            }
        });
    }

    private void vaiParaDetalhesCrenca(Crenca crenca, int posicao) {
        Intent intent = new Intent(getContext(), DetalhesCrencaActivity.class);
        intent.putExtra(CRENCA_ID, crenca.getId());
        intent.putExtra(POSICAO, posicao);
        startActivity(intent);
    }

}
