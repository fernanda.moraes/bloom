package br.com.fernandacosta.bloom.ui.fragment;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import br.com.fernandacosta.bloom.R;
import br.com.fernandacosta.bloom.model.Contato;

import static android.support.v4.content.PermissionChecker.checkSelfPermission;

public class ContatosFragment extends Fragment {

    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contatos, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Contatos");

        FloatingActionButton adicionaFab = view.findViewById(R.id.contatos_adiciona_fab);
        adicionaFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostraContatos();
            }
        });
    }

    private void mostraContatos() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            this.requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            Toast.makeText(getContext(), ", we canot display the names", Toast.LENGTH_SHORT).show();
        } else  {
            Toast.makeText(getContext(), ",asdas names", Toast.LENGTH_SHORT).show();
            escolheContatos();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                escolheContatos();
            } else {
                this.requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }
    }

    private void escolheContatos() {

    }
}
