package br.com.fernandacosta.bloom.ui.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import br.com.fernandacosta.bloom.R;
import br.com.fernandacosta.bloom.dao.CrencaDAO;
import br.com.fernandacosta.bloom.model.Crenca;

import static br.com.fernandacosta.bloom.ui.constant.CrencasConstantes.APPBAR_TITULO_CRENCAS_INTERMEDIARIAS;
import static br.com.fernandacosta.bloom.ui.constant.CrencasConstantes.NUMERO_PERGUNTA;
import static br.com.fernandacosta.bloom.ui.constant.CrencasConstantes.RESPOSTAS_CRENCAS_CENTRAIS;
import static br.com.fernandacosta.bloom.ui.constant.CrencasConstantes.ULTIMA_PERGUNTA;
import static br.com.fernandacosta.bloom.ui.constant.CrencasConstantes.VALOR_INVALIDO;

public class CrencasIntermediariasActivity extends AppCompatActivity {

    private EditText respostaEditText;
    private TextView perguntaTextView;
    private TextView contagemTextView;
    private int numeroPergunta = 0;
    private String[] perguntas;
    private ArrayList<String> respostasCrencasIntermediarias = new ArrayList<>();
    private ArrayList<String> respostasCrencasCentrais;
    CrencaDAO crencaDAO = new CrencaDAO();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crencas_intermediarias);

        setTitle(APPBAR_TITULO_CRENCAS_INTERMEDIARIAS);

        Intent intent = getIntent();

        receberRespostasCrencasCentrais(intent);

        if (editarRespostaJaCriada(intent)) {
            numeroPergunta = intent.getIntExtra(NUMERO_PERGUNTA, VALOR_INVALIDO);
        }

        configuraProximoImageButton();
        inicializaCampos();
        inicializaPerguntas();

        preencheCamposAoIniciar();
    }

    private void receberRespostasCrencasCentrais(Intent intent) {
        if (intent.hasExtra(RESPOSTAS_CRENCAS_CENTRAIS)) {
            respostasCrencasCentrais = intent.getStringArrayListExtra(RESPOSTAS_CRENCAS_CENTRAIS);
        }
    }

    private boolean editarRespostaJaCriada(Intent intent) {
        if (intent.hasExtra(NUMERO_PERGUNTA))
            return true;
        else
            return false;
    }

    public void configuraSalvarImageButton(View view) {
        Crenca crenca = criaCrenca();
        crencaDAO.adicionar(crenca);
        Toast.makeText(getApplicationContext(), "Crenca salva", Toast.LENGTH_SHORT).show();
        finish();
    }

    private Crenca criaCrenca() {
        Crenca crenca = new Crenca();
        crenca.setRespostasCrencasCentrais(crencaDAO.getModelListBoolean(respostasCrencasCentrais));
        crenca.setRespostasCrencasIntermediarias(crencaDAO.getModelList(respostasCrencasIntermediarias));
        crenca.setId(crencaDAO.getUlitmoId());
        crenca.setDiaQueFoiRealizado(Calendar.getInstance().getTime());
        return crenca;
    }

    private void preencheCamposAoIniciar() {
        perguntaTextView.setText(perguntas[numeroPergunta]);
        contagemTextView.setText(formataContagem());
    }

    private void inicializaPerguntas() {
        perguntas = getResources().getStringArray(R.array.crencas_intermediarias);
    }

    private void inicializaCampos() {
        respostaEditText = findViewById(R.id.crencas_intermediarias_resposta_editText);
        perguntaTextView = findViewById(R.id.crencas_intermediarias_pergunta_textView);
        contagemTextView = findViewById(R.id.crencas_intermediarias_contagem_textView);
    }

    private void configuraProximoImageButton() {
        ImageButton proximoImageButton = findViewById(R.id.crencas_intermediarias_proximo_imageButton);
        proximoImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numeroPergunta < ULTIMA_PERGUNTA) {
                    numeroPergunta++;
                    pegaResposta();
                    mudaContagem();
                    mudaPergunta();
                } else if (numeroPergunta == ULTIMA_PERGUNTA) {
                    pegaResposta();
                    setContentView(R.layout.crenca_final);
                }
            }
        });
    }

    private void pegaResposta() {
        String resposta = respostaEditText.getText().toString();
        respostasCrencasIntermediarias.add(resposta);
        limpaCampo();
    }

    private void limpaCampo() {
        respostaEditText.setText("");
    }

    private void mudaPergunta() {
        perguntaTextView.setText(perguntas[numeroPergunta]);
    }

    private void mudaContagem() {
        contagemTextView.setText(formataContagem());
    }

    @NonNull
    private String formataContagem() {
        return String.format(getResources().getString(R.string.crencas_intermediarias_contagem), numeroPergunta + 1);
    }

}
